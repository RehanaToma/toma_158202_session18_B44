
<?php
require_once("../../vendor/autoload.php");

use App\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>information collection form</title>
</head>
<body>

<form action="process.php" method="post">
    enter student's name:
    <input type="text" name="name">
    <br>
    enter student's roll:
    <input type="text" name="roll">
    <br>
    Bangla marks:
    <input type="number" step="any" min="0" max="100" name="markBangla">
    <br>
    English marks:
    <input type="number" step="any" min="0" max="100" name="markEnglish">
    <br>
    Math marks:
    <input type="number" step="any" min="0" max="100" name="markMath">
    <br>
    <input type="submit" >
<script>
    jQuery {
        function ($)
        {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);

        }

    }
</script>


</form>
</body>
</html>